package Processor_top;

import Riscv_types::*;
import Riscv_processor::*;
import Instr_cache_and_main_mem::*;

module mkProcessor_top();

   Ifc_processor riscv_processor <- mkRiscv_processor();
   Ifc_instr_cache_and_main_mem icache_and_main_mem <- mkInstr_cache_and_main_mem();
   
   let memblk_icache_ifc = icache_and_main_mem.ifc_instr_cache;
   let proc_icache_ifc = riscv_processor.ifc_instr_cache;
   let proc_main_mem_ifc = riscv_processor.ifc_main_mem;
   
   Wire#(Icache_data) icache_data <- mkWire();
   Wire#(MainMem_data) mainMem_data <- mkWire();
   
   rule rl_connect_icache_address;
      let icache_addr = proc_icache_ifc._get_pc_value();
      $display("%d: send addr to icache address: %x",$time(),icache_addr);
      icache_data <= memblk_icache_ifc._fetch(icache_addr);
   endrule : rl_connect_icache_address
   
   rule rl_connect_icache_data;
      $display("%d: got data from icache data: %x",$time(),icache_data);
      proc_icache_ifc._put_instr(pack(icache_data));
   endrule : rl_connect_icache_data
   
   rule rl_connect_main_mem_read_addr;
      MainMem_addr lv_mainMem_addr;
      lv_mainMem_addr = proc_main_mem_ifc._get_load_addr();
      $display("%d: send addr to main mem load_addr: %x",$time(),lv_mainMem_addr);
      mainMem_data <= icache_and_main_mem.ifc_main_mem._read(lv_mainMem_addr);
   endrule : rl_connect_main_mem_read_addr
   
   rule rl_connect_main_mem_read_data;
      $display("%d: read_data from main mem main_mem_data: %x",$time(),mainMem_data);
      proc_main_mem_ifc._put_load_data (mainMem_data);
   endrule: rl_connect_main_mem_read_data
   
   rule rl_connect_main_mem_str_data;
      $display("%d: store to main mem",$time());
      MemStr lv_mem_str;
      lv_mem_str = proc_main_mem_ifc._get_str_data();
      icache_and_main_mem.ifc_main_mem._write(lv_mem_str.mainMem_addr,
	 lv_mem_str.mainMem_data);
   endrule
   
endmodule
endpackage
