/*
 * This package defines the main memory and instruction cache of the processor
 * and the corresponding methods to load or store data.
 */


package Instr_cache_and_main_mem;


// Import or include required packages
`include "Riscv_parameters.bsv"
import RegFile::*;
import Riscv_types::*;
import Vector::*;
import ConfigReg::*;
import DReg::*;


// interface declarations

// instruction cache interface declaration
interface Ifc_instr_cache;
   method Icache_data _fetch(Icache_addr icache_addr);
endinterface

// main memory interface declaration
interface Ifc_main_mem;
   method Action _write (MainMem_addr mainMem_addr, MainMem_data mainMem_data);
   method MainMem_data _read (MainMem_addr mainMem_addr);
endinterface

// Declaring interface of above interfaces
interface Ifc_instr_cache_and_main_mem;
   interface Ifc_instr_cache ifc_instr_cache;
   interface Ifc_main_mem ifc_main_mem;
endinterface

// Main memory type definition as ConfigReg vector
typedef Vector#(`MAIN_MEM_SIZE,ConfigReg#(Bit#(32))) ConfigRegFile;



// module starts here
module mkInstr_cache_and_main_mem(Ifc_instr_cache_and_main_mem);
      
   // Instruction cache instantiated as RegFile. Instructions are loaded from 
   // input.hex file
   RegFile#(Icache_addr,Icache_data) icache <- mkRegFileLoad(
	  "input_dependency2.hex",0,fromInteger(valueOf(TSub#(`INSTR_CACHE_SIZE,1))));
   
   // Instantiate main memory
   ConfigRegFile mainMem <- replicateM(mkConfigReg(0));
   
   // Instruction cache interface method
   interface Ifc_instr_cache ifc_instr_cache;
	  
	  // Fetches instruction from instruction cache
	  method Icache_data _fetch (Icache_addr icache_addr);
		 return icache.sub(icache_addr);
      endmethod
   
   endinterface
   
   // Main memory interface methods
   interface Ifc_main_mem ifc_main_mem;
   
	  // Write to main memory with given address and data as inputs 
	  method Action _write (MainMem_addr mainMem_addr,MainMem_data mainMem_data);
		 mainMem[mainMem_addr] <= mainMem_data;
      endmethod
   
	  // Read from main memory from given address
	  method MainMem_data _read (MainMem_addr mainMem_addr);
		 return mainMem[mainMem_addr];
	  endmethod
   endinterface
   
endmodule
endpackage
