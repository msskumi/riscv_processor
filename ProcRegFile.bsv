/*
 * This is the processor's register file. r0 is hardwired to '0' i.e., if the
 * input address is '0', '0' is returned. Also, if a register is written and
 * read in the same cycle, the data is bypassed directly from write port to 
 * read port. We can also use 'CReg' from bluespec library, but the current 
 * implementation uses lesser hardware.
 */


package ProcRegFile;


// Include required parameters from file
`include "Riscv_parameters.bsv"


// Import required libraries
import Vector::*;
import ConfigReg::*;


// Interface declaration
interface Ifc_regFile;
   method Bit#(`REGFILE_WORD_LENGTH) _read(UInt#(TLog#(`REGFILE_SIZE)) address);
   method Action _write(UInt#(TLog#(`REGFILE_SIZE)) address, 
						Bit#(`REGFILE_WORD_LENGTH) data);
endinterface



module mkProcRegFile(Ifc_regFile);

   // Wire to hold the register file address
   Wire#(UInt#(TLog#(`REGFILE_SIZE))) wr_address <- mkDWire(0);
   
   // Wire to hold the register file data
   Wire#(Bit#(`REGFILE_WORD_LENGTH)) wr_data <- mkDWire(0);
   
   // Register file instantiated as a vector of ConfigReg
   Vector#(`REGFILE_SIZE, Reg#(Bit#(`REGFILE_WORD_LENGTH))) 
                                      vec_regFile <- replicateM(mkConfigReg(0));
   
   
   // This method reads the data from register file
   method Bit#(`REGFILE_WORD_LENGTH) _read(UInt#(TLog#(`REGFILE_SIZE)) address);
      
 	  // As r0 is hardwired to '0', 0 is returned if the address is '0'
	  if(address == 0)
		 return 0;
	  
	  // If a register is written and read in the same cycle, then bypass the
	  // data directly from write port to read port
	  else if (wr_address == address)
		 return wr_data;
      
	  // return the data normally in the other cases
	  else
		 return vec_regFile[address];
   endmethod
   
   // This method writes the data into register file
   method Action _write(UInt#(TLog#(`REGFILE_SIZE)) address, 
						Bit#(`REGFILE_WORD_LENGTH) data);
      
	  // wr_address and wr_data are used to bypass the the data from write port
	  // to read port.
	  wr_address <= address;
      wr_data <= data;
      // Write data to register
	  vec_regFile[address] <= data;
   endmethod

endmodule
endpackage
