/*
 * This is the top module of Processor. It connects the main processor pipeline 
 * to instruction cache and main memory. 
 */

package Tb;

// import required packages
import Riscv_types::*;
import Riscv_processor::*;
import Instr_cache_and_main_mem::*;

module mkTb();
   
   /*Instantiate main processor, cache and main memory modules*/
   Ifc_processor riscv_processor <- mkRiscv_processor();
   Ifc_instr_cache_and_main_mem icache_and_main_mem <- mkInstr_cache_and_main_mem();
   
   
   let memblk_icache_ifc = icache_and_main_mem.ifc_instr_cache;
   let proc_icache_ifc = riscv_processor.ifc_instr_cache;
   let proc_main_mem_ifc = riscv_processor.ifc_main_mem;
   
   //Wire to hold instruction from instruction cache
   Wire#(Icache_data) icache_data <- mkWire();
   
   //Wire to hold data from main memory
   Wire#(MainMem_data) mainMem_data <- mkWire();         
   
   

   /*This rule takes the program counter value from processor and gives it to 
	icache. And the instruction from icache is written to a wire.*/
   rule rl_connect_icache_address;
      let icache_addr = proc_icache_ifc._get_pc_value();
      icache_data <= memblk_icache_ifc._fetch(icache_addr);
   endrule : rl_connect_icache_address
   
   

   /*This rule puts the instruction from icache_data wire into the processor.
	 This rule alone with the previous one connects the processor and icache */
   rule rl_connect_icache_data;
      proc_icache_ifc._put_instr(pack(icache_data));
   endrule : rl_connect_icache_data
   
   

   /*Below three rules connects the processor and main memory.*/
   /*This rule gives effective address calculated in the processor to main 
	memory and  writes the main memory data to a wire mainMem_data*/
   rule rl_connect_main_mem_read_addr;
      MainMem_addr lv_mainMem_addr;
      lv_mainMem_addr = proc_main_mem_ifc._get_load_addr();
      mainMem_data <= icache_and_main_mem.ifc_main_mem._read(lv_mainMem_addr);
   endrule : rl_connect_main_mem_read_addr
   

   /*This rule puts the data from main memory which is written to a wire into 
	the processor. This rule along with the above rule reads data from main
	memory*/
   rule rl_connect_main_mem_read_data;
      proc_main_mem_ifc._put_load_data (mainMem_data);
   endrule: rl_connect_main_mem_read_data
   

   /*This rule write the data to be stored into the main memory*/
   rule rl_connect_main_mem_str_data;
      MemStr lv_mem_str;
      lv_mem_str = proc_main_mem_ifc._get_str_data();
      icache_and_main_mem.ifc_main_mem._write(lv_mem_str.mainMem_addr,
	 lv_mem_str.mainMem_data);
   endrule
   
endmodule
endpackage
