package Mem_access;

function fn_mem_access(Valid_executed_instr executed_instr) = 
   action
   
      MainMem_addr lv_mainMem_addr = unpack(executed_instr.alu_result);
      if (executed_instr.instr_type matches LOAD)
	 wr_load_address <= lv_mainMem_addr;
      else if (executed_instr.Instr_type matches STR)
	 begin
	    case (executed_instr.mem_type) matches
	       BYTE:
	       begin
		  wr_str_data <= MemStr {
		     mainMem_addr : lv_mainMem_addr,
		     mainMem_addr : unpack(extend(executed_instr.str_value[7:0]))
		     };
	       end
	       HWORD:
	       begin
		  wr_str_data <= MemStr {
		     mainMem_addr : lv_mainMem_addr,
		     mainMem_addr : unpack(extend(executed_instr.str_value[15:0]))
		     };
	       end
	       WORD:
	       begin
		  wr_str_data <= MemStr {
		     mainMem_addr : lv_mainMem_addr,
		     mainMem_addr : unpack(extend(executed_instr.str_value[15:0]))
		     };
	       end
	    endcase
	 end
      
   endaction;
endpackage
