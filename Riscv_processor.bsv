////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: Riscv Processor                                     //
// Coded by             : M S Santosh Kumar (msskumi@gmail.com)               //
//                      : N Sireesh (sireesh1992@gmail.com)                   //
//                                                                            //
// Module Description	: This module contains the high level description of  //
//                        5-stage RISC-V Processor .                          //
//                                                                            //
// Functionality at a glance :                                                //
//                         1. Five stage pipeline                             //
//                             a) Instruction Fetch                           //
//                             b) Instruction Decode                          //
//                             c) Execute instruction                         //
//                             d) Memory access                               //
//                             e) Write back                                  //
//                         2. Inter-stage operand forwarding                  //
//                                                                            //
// References           : RISC-V Instruction Set Manual                       //
////////////////////////////////////////////////////////////////////////////////

package Riscv_processor;

/* Riscv processor parameter definitions */
`include "Riscv_parameters.bsv"

/* import Bluespec libraries */
import FIFO::*;
import SpecialFIFOs::*;
import DReg::*;

/* import user defined types */
import Riscv_types::*;
import Instr_cache_and_main_mem::*;
import ProcRegFile::*;
import Alu::*;
import Decode::*;

/* Interface declarations: start */

// interface for instruction cache
interface Ifc_instr_cache;
   method Icache_addr _get_pc_value();
   method Action _put_instr(Bit#(32) instr);
endinterface

// interface for main memory or data cache
interface Ifc_main_mem;
   method MainMem_addr _get_load_addr();
   method Action _put_load_data(MainMem_data data);
   method MemStr _get_str_data();
endinterface

// processor interface
interface Ifc_processor;
   interface Ifc_instr_cache ifc_instr_cache;
   interface Ifc_main_mem ifc_main_mem;
   method Action reset_processor();
endinterface

/* Interface declarations: end */

/* Riscv_processor module declaration: start */
module mkRiscv_processor(Ifc_processor);

   /* Reg and Wire declarations: start */
   
   //----reg and wires related to fetch stage----//
   
   // holds fetched instruction
   Wire#(Bit#(32)) wr_fetched_instr <- mkWire();
   // holds program counter
   Reg#(Bit#(32)) rg_pc <- mkReg(0);
   
   //----reg and wires related to mem access stage----//
   
   // holds load address
   Wire#(MainMem_addr) wr_load_address <- mkWire();
   // holds data to be loaded
   Wire#(MainMem_data) wr_load_data <- mkWire();
   // holds data and address of store instruction
   Wire#(MemStr) wr_str_data <- mkWire();
   // flags the instruction as mem access instruction
   Wire#(Bool) dwr_get_data_is_valid <- mkDWire(False);  
   
   //----wires related to inter-stage forwarding logic----//
   
   // holds forwarded rs1 value, if valid
   Wire#(Maybe#(Bit#(`REGFILE_WORD_LENGTH))) maybe_wr_rs1_data <- (
      mkDWire(tagged Invalid));
   // holds forwarded rs2 value, if valid
   Wire#(Maybe#(Bit#(`REGFILE_WORD_LENGTH))) maybe_wr_rs2_data <- (
      mkDWire(tagged Invalid));
   // holds forwared store data, if valid
   Wire#(Maybe#(Bit#(`REGFILE_WORD_LENGTH))) maybe_wr_str_data <- (
      mkDWire(tagged Invalid));
   // asserted if pipeline needs to be stalled
   Wire#(Bool) dwr_stall_pipeline <- mkDWire(False);

   //----reg and wires related to file io----//
   
   // rl_open_dump_file fires when this reg is true. asserted only once on reset
   Reg#(Bool) rg_open_dump_file <- mkReg(True);
   // holds pointer to the dump file
   let rg_dump_file <- mkReg(InvalidFile);
   
   /* Reg and Wire declarations: end */
   
   //----inter-stage pipeline fifos----//
   
   // Pipeline fifo allows enqueue and dequeue concurrently when it is full
   FIFO#(Bit#(32)) ff_fetch_to_decode <- mkPipelineFIFO();   
   FIFO#(Maybe#(Valid_decoded_instr)) ff_decode_to_execute <- mkPipelineFIFO();
   FIFO#(Maybe#(Valid_executed_instr)) ff_execute_to_mem <- mkPipelineFIFO();
   FIFO#(Maybe#(Valid_writeBack_instr)) ff_mem_to_writeback <- mkPipelineFIFO();
   
   // instantiating processor register file
   Ifc_regFile ifc_regFile <- mkProcRegFile();

   /* rule declarations: start */
   
   //----rule that handles file io----//
   
   // rg_open_dump_file is asserted once on reset and deasserted on file open
   rule rl_open_dump_file (rg_open_dump_file);
      String dumpFile = "output.txt";
      File fl_dump <- $fopen( dumpFile, "w");
      if (fl_dump == InvalidFile)
	 begin
	    $finish();
	 end
      rg_open_dump_file <= False;
      rg_dump_file <= fl_dump;
   endrule
   
   /*   Pipeline Stages: Begin    */
   
   // All the rules related to pipeline stages will fire only if their
   // interstage fifos are empty or concurrently dequeued by the next stage.
   // Concurrent dequeuing is usually valid except when pipeline is halted on
   // one condition described at rl_halt_pipeline.
   
   /*    1. Pipeline Stage : Fetch    */
  
   // program counter is presented to instr cache via _get_pc_value() method
   // in instr_cache interface and the data is read into wr_fetched_instr
   // via _put_instr() method. This instr is enqueued into fetch_to_decode fifo.
   
   rule rl_fetch;
      ff_fetch_to_decode.enq(wr_fetched_instr);
      rg_pc <= rg_pc + 1;
   endrule : rl_fetch
   
   /*    2. Pipeline Stage: Decode    */
   
   // fetched instruction is dequeued from fetch_to_decode fifo and
   // presented to combinational logic fn_decode_instr which decodes it.
   // The decoded instruction is then enqueued in decode_to_execute fifo.
   
    rule rl_decode;
       Maybe#(Valid_decoded_instr) lv_decoded_instr;
       Bit#(32) lv_fetched_instr;
       lv_fetched_instr = ff_fetch_to_decode.first();
       ff_fetch_to_decode.deq();
       lv_decoded_instr = fn_decode_instr(lv_fetched_instr,ifc_regFile);
       ff_decode_to_execute.enq(lv_decoded_instr);
    endrule : rl_decode
   
   
   /*    3. Pipeline Stage: Execute    */
   
   // Decode instruction is checked for validity, tagged during decode stage.
   // If valid, the instruction is presented to fn_alu() which executes the
   // instruction and gives back the result. The result is enqueued into
   // execute_to_writeback fifo.
   
   rule rl_execute(!dwr_stall_pipeline);
      Maybe#(Valid_executed_instr) lv_executed_instr;
      Maybe#(Valid_decoded_instr) lv_decoded_instr;
      lv_decoded_instr = ff_decode_to_execute.first();
      ff_decode_to_execute.deq();
      if (lv_decoded_instr matches tagged Valid .lv_valid_decoded_instr)
         begin
	    
	    //  maybe_wr_rsx_data or maybe_wr_str_data is asserted by
	    // the inter-stage forwarding logic. If they are valid, the fowarded
	    // data is used for execution, else the operands fetched in decode
	    // stage is used for execution.
	    // Usage: fromMaybe(Invalid_value, Valid_value)
            Bit#(`REGFILE_WORD_LENGTH) lv_rs1_data = fromMaybe(
	       lv_valid_decoded_instr.rs1_data, maybe_wr_rs1_data);
            Bit#(`REGFILE_WORD_LENGTH) lv_rs2_data = fromMaybe(
	       lv_valid_decoded_instr.rs2_data, maybe_wr_rs2_data);
            Bit#(`REGFILE_WORD_LENGTH) lv_str_data = fromMaybe(
	       lv_valid_decoded_instr.str_value, maybe_wr_str_data);
            
	    // lv_tmp_decoded_instr consists of operand data after forwarding
            Valid_decoded_instr lv_tmp_valid_decoded_instr =  
	       Valid_decoded_instr {
		  rs1: lv_valid_decoded_instr.rs1,
		  rs1_data: lv_rs1_data,
		  rs2: lv_valid_decoded_instr.rs2,
		  rs2_data: lv_rs2_data,
		  rd: lv_valid_decoded_instr.rd,
		  instr_type: lv_valid_decoded_instr.instr_type,
		  op_type: lv_valid_decoded_instr.op_type,
		  mem_type: lv_valid_decoded_instr.mem_type,
		  str_value: lv_str_data
		  };
            
            // the decoded instruction after forwarding is presented to alu
	    // for execution
            lv_executed_instr = tagged Valid fn_alu(lv_tmp_valid_decoded_instr);
         end
      else
	 lv_executed_instr = tagged Invalid;
      ff_execute_to_mem.enq(lv_executed_instr);
      
   endrule : rl_execute
   
   /*    4. Pipeline Stage: Memory Access    */   
   
   // This stage consists of three rules:
   //   a) rl_mem_access_send_addr: If the instruction is valid and it is
   //      LOAD/STR then the corresponding load or store address is sent
   //      to the main_mem/Data_cache for read. Whether the instruction is load
   //      or store, the data is read, and written back in case of store.
   //   b) rl_mem_access_get_data: In the rule the data corresponding the addr
   //      sent in the previous rule is received and processed.
   //   c) rl_no_mem_access: If the instruction is not LOAD/STORE, it is sent to
   //      next stage.
   //   rl_mem_access_send_addr writes True to dwr_get_data_is_valid and the
   //   rl_mem_access_get_data fires only if dwr_get_data_is_valid is True else
   //   rl_no_mem_access is fired.
  
   rule rl_mem_access_send_addr;
      Maybe#(Valid_executed_instr) lv_executed_instr;
      lv_executed_instr = ff_execute_to_mem.first();
      if (lv_executed_instr matches tagged Valid .lv_valid_executed_instr)
		 begin
		    // The effective addr calculated in execute stage is
		    // presented to mainMem/DataCache.
		    MainMem_addr lv_mainMem_addr = unpack(truncate(
		       lv_valid_executed_instr.alu_result));
		    
		    if (lv_valid_executed_instr.instr_type == LOAD || 
			lv_valid_executed_instr.instr_type == STR)
		       begin
			  // mainMem_addr is 32'bit aligned and hence last
			  // two bits are masked.
			  wr_load_address <= lv_mainMem_addr & 32'hFFFFFFFC;
			  dwr_get_data_is_valid <= True;
		       end
		 end
   endrule : rl_mem_access_send_addr
   
   rule rl_mem_access_get_data (dwr_get_data_is_valid);
	  
      if (ff_execute_to_mem.first() matches 
	  tagged Valid .lv_valid_executed_instr)
	     begin
		ff_execute_to_mem.deq();
		MainMem_addr lv_mainMem_addr = (
		   lv_valid_executed_instr.alu_result);
		Valid_writeBack_instr lv_valid_writeback_instr;
		lv_valid_writeback_instr.rd_value = 0;
		lv_valid_writeback_instr.rd = lv_valid_executed_instr.rd;
		if(lv_valid_executed_instr.instr_type == LOAD)
		   begin
		      // if the instr is LOAD, the read mainMem data is sent
		      // to next stage.
		      case (lv_valid_executed_instr.mem_type) matches
			 BYTE:
			 // If the instr is load byte the corresponding
			 // byte from mainMem_data is chosen and sign extended.
			 case (lv_mainMem_addr[1:0])
			    2'b00:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[7:0]);
			    2'b01:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[15:8]);
			    2'b10:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[23:16]);
			    2'b11:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[31:24]);
			 endcase
			 HWORD:
			 // If the instr is load half-word, the appropriate HW
			 // is taken from mainMem_data and sign extended.
			 case (lv_mainMem_addr[1:0])
			    2'b00:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[15:0]);
			    2'b01:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[23:8]);
			    2'b10:
			    lv_valid_writeback_instr.rd_value = signExtend(
			       wr_load_data[31:16]);
			 endcase
			 WORD:
			 // if the instr is load word, the mainMem_data
			 // is directly sent to next stage.
			 lv_valid_writeback_instr.rd_value = wr_load_data;
			 UBYTE:
			 // if the instr is load unsigned byte, the appropriate
			 // byte is chosen from MainMem_data and significant
			 // bits are padded with 0's
			 case (lv_mainMem_addr[1:0])
			    2'b00:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[7:0]);
			    2'b01:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[15:8]);
			    2'b10:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[23:16]);
			    2'b11:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[31:24]);
			 endcase
			 UHWORD:
			 // if the instr is load unsigned HW, the appropriate
			 // byte is chosen from MainMem_data and significant
			 // bits are padded with 0's
			 case (lv_mainMem_addr[1:0])
			    2'b0:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[15:0]);
			    2'b01:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[23:8]);
			    2'b10:
			    lv_valid_writeback_instr.rd_value = extend(
			       wr_load_data[31:16]);
			 endcase
			 default:
			 lv_valid_writeback_instr.rd_value = 0;
		      endcase
		   end
		
		else if(lv_valid_executed_instr.instr_type == STR)
		   begin
		      // if the instr is STR, the read mainMem data is modified
		      // appropriately and written back to mainMem. In case of
		      // store byte, the appropriate byte is replaced and
		      // written back and similarly for half word and word.
		      MainMem_data lv_str_data = 32'b0;
		      case (lv_valid_executed_instr.mem_type)
			 BYTE:
			 begin
			    case(lv_mainMem_addr[1:0])
			       2'b00:
			       lv_str_data = {wr_load_data[31:8],
				  lv_valid_executed_instr.str_value[7:0]};
			       2'b01:
			       lv_str_data = {wr_load_data[31:16],
				  lv_valid_executed_instr.str_value[7:0],
				  wr_load_data[7:0]};
			       2'b10:
			       lv_str_data = {wr_load_data[31:24],
				  lv_valid_executed_instr.str_value[7:0],
				  wr_load_data[15:0]};
			       2'b11:
			       lv_str_data = {
				  lv_valid_executed_instr.str_value[7:0],
				  wr_load_data[23:0]};
			    endcase
			 end
			 HWORD:
			 begin
			    case (lv_mainMem_addr[1:0])
			       2'b00:
			       lv_str_data = {wr_load_data[31:16],
				  lv_valid_executed_instr.str_value[15:0]};
			       2'b01:
			       lv_str_data = {wr_load_data[31:24],
				  lv_valid_executed_instr.str_value[23:8],
				  wr_load_data[7:0]};
			       2'b10:
			       lv_str_data = {
				  lv_valid_executed_instr.str_value[15:0],
				  wr_load_data[15:0]};
			    endcase
			 end
			 WORD:
			 lv_str_data = lv_valid_executed_instr.str_value;
		      endcase
		      // Since the mainMem is 32-bit aligned, the last two
		      // bits are masked for mainMem address.
		      wr_str_data <= MemStr {
			 mainMem_addr : lv_mainMem_addr & 32'hFFFFFFFC,
			 mainMem_data : lv_str_data
			 };
		      // The store address and data is written to the dump file
		      $fwrite(rg_dump_file, "Address = %08h \t Data = %08h \n",
			      lv_mainMem_addr, lv_str_data);
		   end	  
		
		lv_valid_writeback_instr.instr_type = ( 
		   lv_valid_executed_instr.instr_type );
		ff_mem_to_writeback.enq(tagged Valid lv_valid_writeback_instr);
	     end
      
   endrule : rl_mem_access_get_data
   
   rule rl_no_mem_access (!dwr_get_data_is_valid);
      
      Maybe#(Valid_writeBack_instr) lv_writeback_instr;
      Maybe#(Valid_executed_instr) lv_executed_instr;
      
      lv_executed_instr = ff_execute_to_mem.first();
      ff_execute_to_mem.deq();
      if (lv_executed_instr matches tagged Valid .lv_valid_executed_instr)
	 begin 
	    Valid_writeBack_instr lv_valid_writeback_instr;
	    if (lv_valid_executed_instr.instr_type matches ALU)
	       begin
		  // if the instr is an ALU instruction, the result
		  // obtained in execute stage is forwarded to writeback
		  // stage.
		  lv_valid_writeback_instr.rd = lv_valid_executed_instr.rd;
		  lv_valid_writeback_instr.rd_value = (
		     lv_valid_executed_instr.alu_result);
	       end
	    else
	       begin
		  lv_valid_writeback_instr.rd = 0;
		  lv_valid_writeback_instr.rd_value = 0;
	       end
	    lv_valid_writeback_instr.instr_type = (
	       lv_valid_executed_instr.instr_type);
	    lv_writeback_instr = tagged Valid lv_valid_writeback_instr;
	 end
      else
	 begin
	    lv_writeback_instr = tagged Invalid;
	 end
      ff_mem_to_writeback.enq(lv_writeback_instr);
   endrule
   
   /*    5. Pipeline Stage: Write Back    */

   // If the instruction is an ALU/LOAD instruction, the results
   // obtained in execute/mem_access stage are written back to destination
   // register in the register file.
   
   rule rl_write_back;
      
      Maybe#(Valid_writeBack_instr) lv_writeback_instr;
      lv_writeback_instr = ff_mem_to_writeback.first();
      ff_mem_to_writeback.deq();
      if (lv_writeback_instr matches tagged Valid .lv_valid_writeback_instr)
	 begin
	    if ((lv_valid_writeback_instr.instr_type == ALU) ||
		(lv_valid_writeback_instr.instr_type == LOAD) )
	       begin
		  ifc_regFile._write(
		     unpack(lv_valid_writeback_instr.rd),
		     lv_valid_writeback_instr.rd_value);
	       end
	 end
      else
	 $finish();
      
   endrule : rl_write_back

   /* Pipelines stages: end */
   
   
   /* Pipeline halt rule */
   // Execution stage of pipeline is halted when an ALU instruction 
   // is followed by a LOAD instr and one of the operands of the ALU
   // instruction matches the destination register to the which the
   // data is to be loaded since the data in the destination register
   // is available only after the LOAD stage.
   
   rule rl_halt_pipeline;
      
      Valid_executed_instr lv_executed_instr = fromMaybe(
	 ?,ff_execute_to_mem.first());
      Valid_decoded_instr lv_decoded_instr = fromMaybe(
	 ?,ff_decode_to_execute.first());
      Bit#(TLog#(`REGFILE_SIZE)) lv_rs2 = fromMaybe(?,lv_decoded_instr.rs2);
      
      if (  (
             isValid(ff_execute_to_mem.first()) && 
             isValid(ff_decode_to_execute.first())  &&
             lv_executed_instr.instr_type == LOAD
             ) &&
            ( 
             (isValid(lv_decoded_instr.rs2) && lv_executed_instr.rd == lv_rs2) ||
             (lv_executed_instr.rd == lv_decoded_instr.rs1)
             )
         )
         dwr_stall_pipeline <= True;
      
   endrule : rl_halt_pipeline
   
   /* inter-stage forwarding logic: begin */
   // All the operands are forwarded to inputs of alu i.e., to execute stage
   // The operands forwarded from execute_to_mem buffer are more recent than
   // the ones from mem_to_writeback buffer and hence the descending priority.
   (*descending_urgency = "rl_forward_rs1_frm_EtoMem_buffer,rl_forward_rs1_frm_MemtoWB_buffer"*)
   (*descending_urgency = "rl_forward_rs2_frm_EtoMem_buffer,rl_forward_rs2_frm_MemtoWB_buffer"*)

   // This rule forwards the operands from the execute_to_mem buffer.
   // The operands are forwarded from this buffer only if the instruction
   // type is ALU since only the results of alu instructions are available in
   // this buffer.
   rule rl_forward_rs1_frm_EtoMem_buffer (
      isValid(ff_execute_to_mem.first()) && 
      isValid(ff_decode_to_execute.first())  &&
      fromMaybe(?,ff_execute_to_mem.first()).instr_type == ALU &&
      fromMaybe(?,ff_execute_to_mem.first()).rd == fromMaybe(?,ff_decode_to_execute.first()).rs1
          );
      Valid_executed_instr lv_executed_instr = fromMaybe(
	 ?,ff_execute_to_mem.first());
	  if(fromMaybe(?,ff_execute_to_mem.first()).rd == 0) maybe_wr_rs1_data <= tagged Valid 0;  else maybe_wr_rs1_data <= tagged Valid  lv_executed_instr.alu_result;
  
   endrule : rl_forward_rs1_frm_EtoMem_buffer

   // This rule forwards the operand rs2 from execute to mem buffer.
   rule rl_forward_rs2_frm_EtoMem_buffer ( 
      isValid(ff_execute_to_mem.first()) && 
      isValid(ff_decode_to_execute.first())  &&
      isValid( fromMaybe(?,ff_decode_to_execute.first()).rs2) &&
      fromMaybe(?,ff_execute_to_mem.first()).instr_type == ALU &&
      fromMaybe(?,ff_execute_to_mem.first()).rd == fromMaybe(?,
         fromMaybe(?,ff_decode_to_execute.first()).rs2)
      );
      Valid_executed_instr lv_executed_instr = fromMaybe(
	 ?,ff_execute_to_mem.first());
      Valid_decoded_instr lv_decoded_instr = fromMaybe(
	 ?,ff_decode_to_execute.first());
      
      if(lv_decoded_instr.instr_type==ALU)
		 if(fromMaybe(?,ff_execute_to_mem.first()).rd == 0) maybe_wr_rs2_data <= tagged Valid 0; else maybe_wr_rs2_data <= tagged Valid  lv_executed_instr.alu_result;
      else if(lv_decoded_instr.instr_type==STR)
		 if(fromMaybe(?,ff_execute_to_mem.first()).rd == 0) maybe_wr_str_data <= tagged Valid 0; else maybe_wr_str_data <= tagged Valid  lv_executed_instr.alu_result;

   endrule : rl_forward_rs2_frm_EtoMem_buffer

   // This rule forwards operand rs1 from mem_to_writeback buffer.
   // The operands are forwarded from these buffers only if the instruction
   // types are ALU or LOAD instructions.
   rule rl_forward_rs1_frm_MemtoWB_buffer  (
          isValid(ff_mem_to_writeback.first()) && 
          isValid(ff_decode_to_execute.first())  &&
          (fromMaybe(?,ff_mem_to_writeback.first()).instr_type == ALU || 
           fromMaybe(?,ff_mem_to_writeback.first()).instr_type == LOAD) &&
          fromMaybe(?,ff_mem_to_writeback.first()).rd  == fromMaybe(?,ff_decode_to_execute.first()).rs1
          );
      Valid_writeBack_instr lv_writeback_instr = fromMaybe(
	 ?,ff_mem_to_writeback.first());
	  
	  if(fromMaybe(?,ff_mem_to_writeback.first()).rd == 0)
		 maybe_wr_rs1_data <= tagged Valid  0; else maybe_wr_rs1_data <= tagged Valid  lv_writeback_instr.rd_value;
   
   endrule : rl_forward_rs1_frm_MemtoWB_buffer

   // This rule forwards operand rs2 from mem_to_writeback buffer.
   rule rl_forward_rs2_frm_MemtoWB_buffer  (
      isValid(ff_mem_to_writeback.first()) && 
      isValid(ff_decode_to_execute.first())  &&
      isValid(fromMaybe(?,ff_decode_to_execute.first()).rs2) &&
      (fromMaybe(?,ff_mem_to_writeback.first()).instr_type == ALU || 
       fromMaybe(?,ff_mem_to_writeback.first()).instr_type == LOAD) &&
       (fromMaybe(?,ff_mem_to_writeback.first()).rd == fromMaybe(?,
         fromMaybe(?,ff_decode_to_execute.first()).rs2))       
      );
      Valid_writeBack_instr lv_writeback_instr = fromMaybe(
	 ?,ff_mem_to_writeback.first());
      Valid_decoded_instr lv_decoded_instr = fromMaybe(
	 ?,ff_decode_to_execute.first());
      if(lv_decoded_instr.instr_type==ALU)
         if(fromMaybe(?,ff_mem_to_writeback.first()).rd == 0)
			maybe_wr_rs2_data <= tagged Valid  0; else maybe_wr_rs2_data <= tagged Valid  lv_writeback_instr.rd_value;
		 
      else if(lv_decoded_instr.instr_type==STR)
		 if(fromMaybe(?,ff_mem_to_writeback.first()).rd == 0)
			maybe_wr_str_data <= tagged Valid  0; else maybe_wr_str_data <= tagged Valid  lv_writeback_instr.rd_value;
   endrule

   /* rule declarations: end */
   
   /* interface declarations: start */
   
   // interface to instruction cache.
   interface Ifc_instr_cache ifc_instr_cache;

   // value of program counter is provided by this method
   method Icache_addr _get_pc_value();
      return unpack(truncate(rg_pc));
   endmethod
      
   // instruction corresponding to PC is to be provided through this method
   method Action _put_instr(Bit#(32) instr);
      wr_fetched_instr <= instr;
   endmethod

   endinterface
   
   // interface to main memory (or) data cache
   interface Ifc_main_mem ifc_main_mem;
      
      // the load address is provided by this interface.
      method MainMem_addr _get_load_addr();
		 return wr_load_address;
      endmethod
      
      // the load data is to be provided through this method.
      method Action _put_load_data(MainMem_data data);
		 wr_load_data <= data;
      endmethod
   
      // the store address and data is provided by this method.
      method MemStr _get_str_data();
		 return wr_str_data;
      endmethod
   
   endinterface
   
   // method to reset the processor provided for external use.
   // this method is not used in this implementation.
   method Action reset_processor();
      rg_pc <= 0;
      ff_fetch_to_decode.clear();
      ff_decode_to_execute.clear();
      ff_execute_to_mem.clear();
	  ff_mem_to_writeback.clear();
   endmethod

endmodule
endpackage
