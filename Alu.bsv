////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: Riscv Processor ALU                                 //
// Coded by             : M S Santosh Kumar (msskumi@gmail.com)               //
//                      : N Sireesh (sireesh1992@gmail.com)                   //
//                                                                            //
// Module Description	: This module implements the ALU block of riscv       //
//                        Processor.                                          //
//                                                                            //
// Supported operations : ADD, SUB, MUL, MULH, SLL, SRL, SRA, SLTU, SLT, AND, //
//                        OR, XOR                                             //
//                                                                            //
// References           : RISC-V Instruction Set Manual                       //
////////////////////////////////////////////////////////////////////////////////

package Alu;

`include "Riscv_parameters.bsv"
import Riscv_types::*;

function Valid_executed_instr fn_alu(Valid_decoded_instr decoded_instr);
   
   Valid_executed_instr lv_executed_instr = Valid_executed_instr{
      alu_result	: 0,
      instr_type	: decoded_instr.instr_type,
      mem_type		: decoded_instr.mem_type,
      rd		: decoded_instr.rd,
      str_value		: decoded_instr.str_value
      };
   
   let op_type = decoded_instr.op_type;
   let op1_value = decoded_instr.rs1_data;
   let op2_value = decoded_instr.rs2_data;
   case(decoded_instr.op_type) matches
      ADD:
      begin
	 Int#(`REGFILE_WORD_LENGTH) op1_value_signed = unpack(op1_value);
	 Int#(`REGFILE_WORD_LENGTH) op2_value_signed = unpack(op2_value);
	 lv_executed_instr.alu_result = pack(
	    op1_value_signed + op2_value_signed);
      end
      SUB:
      begin
	 Int#(`REGFILE_WORD_LENGTH) op1_value_signed = unpack(op1_value);
	 Int#(`REGFILE_WORD_LENGTH) op2_value_signed = unpack(op2_value);
	 lv_executed_instr.alu_result = pack(
	    op1_value_signed - op2_value_signed);
      end
      MUL:
      begin
	 // MUL returns least significant bits after multiplication
         Int#(`REGFILE_WORD_LENGTH) op1_value_signed = unpack(op1_value);
         Int#(`REGFILE_WORD_LENGTH) op2_value_signed = unpack(op2_value);
         lv_executed_instr.alu_result = pack(
	    op1_value_signed * op2_value_signed);
      end
      MULH:
      begin
	 // MULH returns most significant bits after multiplication
         Int#(TMul#(2,`REGFILE_WORD_LENGTH)) op1_value_signed = signExtend(
	    unpack(op1_value));
         Int#(TMul#(2,`REGFILE_WORD_LENGTH)) op2_value_signed = signExtend(
	    unpack(op2_value));
         lv_executed_instr.alu_result = pack(
	    op1_value_signed*op2_value_signed)[63:32];
      end
      SLL: // SLL corresponds to logical shift left
      lv_executed_instr.alu_result = op1_value << op2_value;
      SRL: // SLL corresponds to logical shift right
      lv_executed_instr.alu_result = op1_value >> op2_value;
      SRA:
      begin
	 // SRA corresponds to arithmetic shift right.
	 // In bluespec, shift of Int type operand results in arithmetic shift
	 //  and hence the type cast
	 Int#(`REGFILE_WORD_LENGTH) op1_value_signed = unpack(op1_value);
	 lv_executed_instr.alu_result = pack(op1_value_signed >> op2_value);
      end
      SLTU:
      begin
	 // SLTU correponds to set less than unsigned.
	 if(op1_value < op2_value)
	    lv_executed_instr.alu_result = 1;
	 else
	    lv_executed_instr.alu_result = 0;
      end
      SLT:
      begin
	 // SLT corresponds to set less than signed.
	 Int#(`REGFILE_WORD_LENGTH) op1_value_signed = unpack(op1_value);
	 Int#(`REGFILE_WORD_LENGTH) op2_value_signed = unpack(op2_value);
	 if (op1_value < op2_value)
	    lv_executed_instr.alu_result = 1;
	 else
	    lv_executed_instr.alu_result = 0;
      end
      AND:
      lv_executed_instr.alu_result = op1_value & op2_value;
      OR:
      lv_executed_instr.alu_result = op1_value | op2_value;
      XOR:
      lv_executed_instr.alu_result = op1_value ^ op2_value;
      default:
      lv_executed_instr.alu_result = 0;
   
   endcase
   return lv_executed_instr;
endfunction

endpackage
