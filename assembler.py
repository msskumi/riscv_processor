from pylab import*
import re

def get_reg_index(reg_str):
    reg_int = int(reg_str[1:])
    return '{0:05b}'.format(reg_int)


def imm(imm_str):
    imm_int = int(imm_str[1:])
    imm_int_signed = imm_int if (imm_int >= 0) else ((0x100000000+imm_int) & 0x00000FFF )
    return '{0:012b}'.format(imm_int_signed)

def print_as_hex(instr_str):
    instr_int = int(instr_str,2)
    print '{0:08x}'.format(instr_int)

def lshift(val, n): return val>>n if val >= 0 else (val+0x100000000)>>n

opcode = 1111111;
main_mem = [0] * 100;
reg_file = [0] * 32;
inp_file = open("input.asm","r")
out_file = open("output_golden.txt","w")

for l in inp_file.readlines():
    instr_seq = l.split()
    #print instr_seq
    if (instr_seq[0] == "LB"):

        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "000" + str(get_reg_index(instr_seq[1])) + "0000011")
        address = reg_file[(int(instr_seq[2][1:]))] + int(instr_seq[3][1:])
        if (address & 0x000000FF == 0):
            temp1 = main_mem[address>>2] & 0x000000FF
        elif (address & 0x000000FF == 1):
            temp1 = (main_mem[address>>2] & 0x0000FF00) >> 8
        elif (address & 0x000000FF == 2):
            temp1 = (main_mem[address>>2] & 0x00FF0000) >> 16
        elif (address & 0x000000FF == 3):
            temp1 = (main_mem[address>>2] & 0x00FF0000) >> 24
        temp = temp1 & 0x000000FF
        if ((temp >> 7) == 1):
            temp = temp | FFFFFF00
            reg_file[(int(instr_seq[1][1:]))] = temp

    elif (instr_seq[0] == "LH"):

        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "001" + str(get_reg_index(instr_seq[1])) + "0000011")
        address = reg_file[(int(instr_seq[2][1:]))] + int(instr_seq[3][1:])
        if (address & 0x000000FF == 0):
            temp1 = main_mem[address>>2] & 0x0000FFFF
        elif (address & 0x000000FF == 1):
            temp1 = (main_mem[address>>2] & 0x00FFFF00) >> 8
        elif (address & 0x000000FF == 2):
            temp1 = (main_mem[address>>2] & 0xFFFF0000) >> 16

        temp = temp1 & 0x0000FFFF
        if ((temp >> 15) == 1):
            temp = temp | FFFF0000;
        reg_file[(int(instr_seq[1][1:]))] = temp

    elif (instr_seq[0] == "LW"):

        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "010" + str(get_reg_index(instr_seq[1])) + "0000011")
        address = reg_file[(int(instr_seq[2][1:]))] + int(instr_seq[3][1:])
        reg_file[(int(instr_seq[1][1:]))] = main_mem[address>>2]

    elif (instr_seq[0] == "LBU"):
        
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "100" + str(get_reg_index(instr_seq[1])) + "0000011")
        address = reg_file[(int(instr_seq[2][1:]))] + int(instr_seq[3][1:])
        if (address & 0x000000FF == 0):
            temp1 = main_mem[address>>2] & 0x000000FF
        elif (address & 0x000000FF == 1):
            temp1 = (main_mem[address>>2] & 0x0000FF00) >> 8
        elif (address & 0x000000FF == 2):
            temp1 = (main_mem[address>>2] & 0x00FF0000) >> 16
        elif (address & 0x000000FF == 3):
            temp1 = (main_mem[address>>2] & 0x00FF0000) >> 24

        reg_file[(int(instr_seq[1][1:]))] = temp1
    
    elif (instr_seq[0] == "LHU"):
        
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "101" + str(get_reg_index(instr_seq[1])) + "0000011")
        address = reg_file[(int(instr_seq[2][1:]))] + int(instr_seq[3][1:])
        if (address & 0x000000FF == 0):
            temp1 = main_mem[address>>2] & 0x0000FFFF
        elif (address & 0x000000FF == 1):
            temp1 = (main_mem[address>>2] & 0x00FFFF00) >> 8
        elif (address & 0x000000FF == 2):
            temp1 = (main_mem[address>>2] & 0xFFFF0000) >> 16
        reg_file[(int(instr_seq[1][1:]))] = temp1
   
    elif (instr_seq[0] == "SB"):
        
        print_as_hex(imm(instr_seq[3])[0:7] + str(get_reg_index(instr_seq[2])) + str(get_reg_index(instr_seq[1])) + "000" + imm(instr_seq[3])[7:] + "0100011")
        address = reg_file[(int(instr_seq[1][1:]))] + int(instr_seq[3][1:])
        main_mem[address] = reg_file[(int(instr_seq[2][1:]))] & 0x000000FF
        if (address & 0x00000003 == 0):
            main_mem[address>>2] = (main_mem[address>>2] & 0xFFFFFF00) | (reg_file[(int(instr_seq[2][1:]))] & 0x000000FF)
        elif (address & 0x00000003 == 1):
            main_mem[address>>2] = (main_mem[address>>2] & 0xFFFF00FF) | ((reg_file[(int(instr_seq[2][1:]))] & 0x000000FF) << 8)
        elif (address & 0x00000003 == 2):
            main_mem[address>>2] = (main_mem[address>>2] & 0xFF00FFFF) | ((reg_file[(int(instr_seq[2][1:]))] & 0x000000FF) << 16)
        elif (address & 0x00000003 == 3):
            main_mem[address>>2] = (main_mem[address>>2] & 0x00FFFFFF) | ((reg_file[(int(instr_seq[2][1:]))] & 0x000000FF) << 24)

        out_file.write( "Address: " + str(hex(address)) + " Data: "+str(hex(main_mem[address>>2]))+"\n")
    
    elif (instr_seq[0] == "SH"):

        print_as_hex(imm(instr_seq[3])[0:7] + str(get_reg_index(instr_seq[2])) + str(get_reg_index(instr_seq[1])) + "001" + imm(instr_seq[3])[7:] + "0100011")
        address = reg_file[(int(instr_seq[1][1:]))] + int(instr_seq[3][1:])
        if (address & 0x00000003 == 0):
            main_mem[address>>2] = (main_mem[address>>2] & 0xFFFF0000) | (reg_file[(int(instr_seq[2][1:]))] & 0x0000FFFF)
        elif (address & 0x00000003 == 1):
            main_mem[address>>2] = (main_mem[address>>2] & 0xFF0000FF) | ((reg_file[(int(instr_seq[2][1:]))] & 0x0000FFFF) << 8)
        elif (address & 0x00000003 == 2):
            main_mem[address>>2] = (main_mem[address>>2] & 0x0000FFFF) | ((reg_file[(int(instr_seq[2][1:]))] & 0x0000FFFF) << 16)
        out_file.write( "Address: " + str(hex(address)) + " Data: "+str(hex(main_mem[address>>2]))+"\n")

    elif (instr_seq[0] == "SW"):
       
        print_as_hex(imm(instr_seq[3])[0:7] + str(get_reg_index(instr_seq[2])) + str(get_reg_index(instr_seq[1])) + "010" + imm(instr_seq[3])[7:] + "0100011")
        address = reg_file[(int(instr_seq[1][1:]))] + int(instr_seq[3][1:])
        main_mem[address >> 2] = reg_file[(int(instr_seq[2][1:]))]
        out_file.write( "Address: " + str(hex(address)) + " Data: "+str(hex(main_mem[address>>2]))+"\n")
    
    elif (instr_seq[0] == "ADDI"):
      
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "000" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] + int(instr_seq[3][1:])
        
    elif (instr_seq[0] == "SLTI"):

        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "010" + str(get_reg_index(instr_seq[1])) + "0010011")
        if (reg_file[int(instr_seq[2][1:])] < int(instr_seq[3][1:])):
            reg_file[int(instr_seq[1][1:])] = 1
        else:
            reg_file[int(instr_seq[1][1:])] = 0
        
    elif (instr_seq[0] == "SLTIU"):
        
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "011" + str(get_reg_index(instr_seq[1])) + "0010011")
        if ((reg_file[int(instr_seq[2][1:])]) & 0xFFFFFFFF < (int(instr_seq[3][1:])) & 0xFFFFFFFF):
            reg_file[int(instr_seq[1][1:])] = 1
        else:
            reg_file[int(instr_seq[1][1:])] = 0

    
    elif (instr_seq[0] == "XORI"):
        
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "100" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] ^ int(instr_seq[3][1:])

    elif (instr_seq[0] == "ORI"):
       
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "110" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] | int(instr_seq[3][1:])

    elif (instr_seq[0] == "ANDI"):
        
        print_as_hex(imm(instr_seq[3]) + str(get_reg_index(instr_seq[2])) + "111" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] & int(instr_seq[3][1:])

    elif (instr_seq[0] == "SLLI"):
        
        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "001" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] << int(instr_seq[3][1:])    

    elif (instr_seq[0] == "SRLI"):

        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "101" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = lshift(reg_file[int(instr_seq[2][1:])], int(instr_seq[3][1:]))

    elif (instr_seq[0] == "SRAI"):

        print_as_hex("0100000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "101" + str(get_reg_index(instr_seq[1])) + "0010011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] >> int(instr_seq[3][1:])

    elif (instr_seq[0] == "ADD"):

        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "000" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] + reg_file[int(instr_seq[3][1:])]

    elif (instr_seq[0] == "SUB"):

        print_as_hex("0100000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "000" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] - reg_file[int(instr_seq[3][1:])]

    elif (instr_seq[0] == "SLL"):

        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "001" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] >> reg_file[int(instr_seq[3][1:])]

    elif (instr_seq[0] == "SLT"):
       
        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "010" + str(get_reg_index(instr_seq[1])) + "0110011")
        if (reg_file[int(instr_seq[2][1:])] < reg_file[int(instr_seq[3][1:])]):
            reg_file[int(instr_seq[1][1:])] = 1
        else:
            reg_file[int(instr_seq[1][1:])] = 0

    elif (instr_seq[0] == "SLTU"):

        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "011" + str(get_reg_index(instr_seq[1])) + "0110011")
        if ((reg_file[int(instr_seq[2][1:])]) & 0xFFFFFFFF < (reg_file[int(instr_seq[3][1:])] & 0xFFFFFFFF)):
            reg_file[int(instr_seq[1][1:])] = 1
        else:
            reg_file[int(instr_seq[1][1:])] = 0


    elif (instr_seq[0] == "XOR"):

        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "100" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] - reg_file[int(instr_seq[3][1:])]

    elif (instr_seq[0] == "SRL"):
        
        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "101" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = lshift(reg_file[int(instr_seq[2][1:])], reg_file[int(instr_seq[3][1:])])

    elif (instr_seq[0] == "SRA"):
    
        print_as_hex("0100000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "101" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] >> reg_file[int(instr_seq[3][1:])]
    
    elif (instr_seq[0] == "OR"):
    
        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "110" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] | reg_file[int(instr_seq[3][1:])]
    
    elif (instr_seq[0] == "AND"):
    
        print_as_hex("0000000" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "111" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = reg_file[int(instr_seq[2][1:])] | reg_file[int(instr_seq[3][1:])]
    
    elif (instr_seq[0] == "MUL"):
    
        print_as_hex("0000001" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "000" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = (reg_file[int(instr_seq[2][1:])] * reg_file[int(instr_seq[3][1:])]) & 0x0000FFFF
    
    elif (instr_seq[0] == "MULH"):
    
        print_as_hex("0000001" + str(get_reg_index(instr_seq[3])) + str(get_reg_index(instr_seq[2])) + "001" + str(get_reg_index(instr_seq[1])) + "0110011")
        reg_file[int(instr_seq[1][1:])] = (reg_file[int(instr_seq[2][1:])] - reg_file[int(instr_seq[3][1:])]) & 0xFFFF0000


print "aaaaaaaa"













                                        
        
    
