/*
 * This is instruction decoder which takes in the 32 bit instruction, decodes
 * it and outputs the decoded instruction as 'Valid_decoded_instr' type defined
 * in riscv_types package. Decoded instruction is declared as 'Maybe' type, if 
 * the decoded instruction is not valid, it is tagged 'Invalid' or it is tagged
 * 'Valid' with Valid_decoded_instr struct defined in "riscv_types.bsv" 
 */


package Decode;

// import the required packages
import Riscv_types::*;
`include "Riscv_parameters.bsv"
import ProcRegFile::*;

// Decoder defined as a function
function Maybe#(Valid_decoded_instr) fn_decode_instr(Bit#(32) instr, 
													 Ifc_regFile ifc_regfile);
   
   // Unknown instruction is tagged invalid
   Maybe#(Valid_decoded_instr) decoded_instr = tagged Invalid;
   
   
   // Load instruction decode
   if(instr[6:0]=='b0000011)
      begin
         Bit#(`REGFILE_WORD_LENGTH) lv_rs1_data = 
		                                ifc_regfile._read(unpack(instr[19:15]));
         Mem_type lv_mem_type = NOP;
         
		 // Mem_type refers to type of the data to be loaded
		 case(instr[14:12]) matches
            'b000:    lv_mem_type = BYTE;
            'b001:    lv_mem_type = HWORD;
            'b010:    lv_mem_type = WORD;
            'b100:    lv_mem_type = UBYTE;
            'b101:    lv_mem_type = UHWORD;
         endcase
         
		 // Decoded load instruction
		 Valid_decoded_instr valid_decoded_instr = Valid_decoded_instr{
			rs1			: instr[19 :15],
            rs1_data	: lv_rs1_data,
            rs2			: tagged Invalid,
            rs2_data	: signExtend(instr[31:20]),
            rd			: instr[11:7],
            instr_type	: LOAD,
            op_type		: ADD,
            mem_type	: lv_mem_type,
            str_value	: 0
            };
         decoded_instr = tagged Valid valid_decoded_instr;
      end
         
   
   // Store instruction decode
   else if(instr[6:0]=='b0100011)
      begin
         Bit#(`REGFILE_WORD_LENGTH) lv_rs1_data =  
		                                ifc_regfile._read(unpack(instr[19:15]));
         Bit#(`REGFILE_WORD_LENGTH) lv_str_value = 
                                        ifc_regfile._read(unpack(instr[24:20]));
         Mem_type lv_mem_type = NOP;

		 // Mem_type refers to type of data to be stored
         case(instr[14:12]) matches
            'b000: lv_mem_type = BYTE;
            'b001: lv_mem_type = HWORD;
            'b010: lv_mem_type = WORD;
         endcase
		 
		 // Decoded store instruction
         Valid_decoded_instr valid_decoded_instr = Valid_decoded_instr{
            rs1: instr[19:15],
            rs1_data:lv_rs1_data,
            rs2: tagged Valid instr[24:20],
            rs2_data:signExtend({instr[31:25],instr[11:7]}),
            rd:0,
            instr_type: STR,
            op_type: ADD,
            mem_type: lv_mem_type,
            str_value: lv_str_value
            };
         decoded_instr = tagged Valid valid_decoded_instr;
      end
   
   // Decode ALU instruction with immediate value as one operand
   else if(instr[6:0]=='b0010011)
      begin
         Bit#(`REGFILE_WORD_LENGTH) lv_rs1_data =  
                                        ifc_regfile._read(unpack(instr[19:15]));
         Op_type lv_op_type = NOP;
         Bit#(`REGFILE_WORD_LENGTH) lv_rs2_data = 0;
		 
		 // ALU function determined according to instr[14:12] value
         case(instr[14:12]) matches
            'b000: begin 
                      lv_op_type = ADD;
                      lv_rs2_data = signExtend(instr[31:20]);
                   end
            'b001: begin
                      lv_op_type = SLL;
                      lv_rs2_data = extend(instr[24:20]);
                   end
            'b010: begin
                      lv_op_type = SLT;
                      lv_rs2_data = extend(instr[31:20]);
                   end
            'b011: begin
                      lv_op_type = SLTU;
                      lv_rs2_data = signExtend(instr[31:20]);
                   end
            'b100: begin
                      lv_op_type = XOR;
                      lv_rs2_data = signExtend(instr[31:20]);
                   end
            'b101: begin
                      lv_rs2_data = extend(instr[24:20]);
                      if(instr[30] == 'b1)
                         lv_op_type = SRA;
                      else
                         lv_op_type = SRL;
                   end                      
            'b110: begin
                      lv_op_type = OR;
                      lv_rs2_data = signExtend(instr[31:20]);
                   end
            'b111: begin
                      lv_op_type = AND;
                      lv_rs2_data = signExtend(instr[31:20]);
                   end
         endcase

		 // Decoded ALU instruction
         Valid_decoded_instr valid_decoded_instr = Valid_decoded_instr{
            rs1:instr[19:15],
            rs1_data:lv_rs1_data,
            rs2:tagged Invalid,
            rs2_data:lv_rs2_data,
            rd:instr[11:7],
            instr_type: ALU,
            op_type: lv_op_type,
            mem_type: NOP,
            str_value: 0
            };
         decoded_instr = tagged Valid valid_decoded_instr;  
      end
   
   
   // Decode ALU instruction with registers as both operands
   else if(instr[6:0] == 'b0110011)
      begin
         Bit#(`REGFILE_WORD_LENGTH) lv_rs1_data =  
                                        ifc_regfile._read(unpack(instr[19:15]));
         Bit#(`REGFILE_WORD_LENGTH) lv_rs2_data =  
                                        ifc_regfile._read(unpack(instr[24:20]));
         Op_type lv_op_type = NOP;
		 
		 // ALU operation type determined by instr[14:12] value
         case(instr[14:12]) matches
            'b000: begin
                      if(instr[25]=='b0)
                         begin
                            if(instr[30] == 'b0)
                               lv_op_type = ADD;
                            else
                               lv_op_type = SUB;
                         end
                      else
                         lv_op_type = MUL;
                   end
            'b001: begin
                      if(instr[25]=='b0)
						 begin
							lv_op_type = SLL;
							lv_rs2_data = extend(lv_rs2_data[4:0]);
						 end
                      else
                         lv_op_type = MULH;
                   end
            'b010: lv_op_type = SLT;
            'b011: lv_op_type = SLTU;
            'b100: lv_op_type = XOR;
			'b101: begin
                      if(instr[30] == 'b1)
						 begin
							lv_op_type = SRA;
							lv_rs2_data = extend(lv_rs2_data[4:0]);
						 end
					  else
						 begin
							lv_op_type = SRL;
						    lv_rs2_data = extend(lv_rs2_data[4:0]);
						 end
				   end            
			'b110: lv_op_type = OR;
            'b111: lv_op_type = AND;
         endcase
		 
		 // Decoded ALU instruction
         Valid_decoded_instr valid_decoded_instr = Valid_decoded_instr{
            rs1:instr[19:15],
            rs1_data: lv_rs1_data,
            rs2: tagged Valid instr[24:20],
            rs2_data: lv_rs2_data,
            rd: instr[11:7],
            instr_type: ALU,
            op_type: lv_op_type,
            mem_type: NOP,
            str_value: 0
            };
         decoded_instr = tagged Valid valid_decoded_instr;
      end
   
   // Return the decoded instruction
   return decoded_instr;
   
endfunction

endpackage 
