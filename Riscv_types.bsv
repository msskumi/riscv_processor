/*
 * This package defines the all the enums and structs used in the processor.
 */


package Riscv_types;

// Include the required parameters from Riscv_parameters.bsv
`include "Riscv_parameters.bsv"



/* Operation type is defined as enum. NOP represents invalid op type */
typedef enum {
   ADD,SUB,MUL,SLL,SRL,SRA,SLT,SLTU,AND,OR,XOR,MULH,NOP
   }Op_type deriving(Bits, Eq);




/*Instruction type is defined as enum. NOP represents invalid Instruction type*/
typedef enum {
   ALU, LOAD, STR, NOP
   }Instr_type deriving(Bits, Eq);




/* Size of the operand to be loaded or stored is defined as enum. NOP represents
   invalid size */
typedef enum {
   BYTE,HWORD,WORD,UBYTE,UHWORD,NOP
   }Mem_type deriving(Bits, Eq);



// Instruction cache address type
typedef Bit#(TLog#(`INSTR_CACHE_SIZE)) Icache_addr;

// Instruction cache data type
typedef Bit#(`ICACHE_WORD_LENGTH) Icache_data;

// Main memory address type
typedef Bit#(32) MainMem_addr;

// Main memory data type
typedef Bit#(`MAIN_MEM_WORD_LENGTH) MainMem_data;





/* Valid decoded instruction type */
typedef struct {   

   // Operand 1 address (tag) in register file
   Bit#(TLog#(`REGFILE_SIZE))                 rs1;

   // Operand 1 data
   Bit#(`REGFILE_WORD_LENGTH)                 rs1_data;

   // Operand 2 address (tag) in register file. It is a 'Maybe' type which is
   // tagged 'Invalid' in the instructions with no rs2 operand (instructions 
   // with immediate value, load instructions)
   Maybe#(Bit#(TLog#(`REGFILE_SIZE)))         rs2;

   // Operand 2 data. It also holds the sign extended immediate values.
   Bit#(`REGFILE_WORD_LENGTH)                 rs2_data;
   
   // Destination register address (tag) in register file
   Bit#(TLog#(`REGFILE_SIZE))                 rd;
   
   // Holds instruction type (defined above)
   Instr_type                                 instr_type;
   
   // Holds operation type (defined above)
   Op_type                                    op_type;
   
   // Holds the size of the operand to be stored or loaded
   Mem_type                                   mem_type;
   
   // Value to be stored (in store instructions) 
   Bit#(`REGFILE_WORD_LENGTH)                 str_value;

   }Valid_decoded_instr deriving(Bits, Eq);





/* Valid exectuted instruction type. Invalid instructions from decode stage are
   tagged 'Invalid' in execution stage as well. */  
typedef struct {
   
   // Holds ALU result or Effective address in the case of load store 
   // instructions 
   Bit#(`REGFILE_WORD_LENGTH) alu_result;
   
   // Instruction type
   Instr_type instr_type;
   
   // Size of the operand to be stored or loaded
   Mem_type mem_type;
   
   // Destination address (tag) in register file
   Bit#(TLog#(`REGFILE_SIZE)) rd;
   
   // Hold the value to be stored in the case of store instructions
   Bit#(`REGFILE_WORD_LENGTH) str_value;

   }Valid_executed_instr deriving(Bits, Eq);





/* Write back instruction type. It is valid only if the execution instruction
   type is also 'Valid'. */
typedef struct {
   
   // Destination register address (tag) in register file
   Bit#(TLog#(`REGFILE_SIZE)) rd;
   
   // value to be stored in destination register
   Bit#(`REGFILE_WORD_LENGTH) rd_value;
   
   // Instruction type (rd is written only in ALU and LOAD instructions)
   Instr_type instr_type;
   
   }Valid_writeBack_instr deriving(Bits, Eq);




/* Struct having the data needed in store operation */ 
typedef struct {
   MainMem_addr mainMem_addr;
   MainMem_data mainMem_data;
   }MemStr deriving(Bits,Eq);

endpackage
